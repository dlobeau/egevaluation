package com.EGEvaluation.carRegistryservice;

import com.EGEvaluation.carRegistryservice.controller.CarRegistryController;
import com.EGEvaluation.carRegistryservice.dto.CarRequest;
import com.EGEvaluation.carRegistryservice.dto.CarResponse;
import com.EGEvaluation.carRegistryservice.service.CarRegistryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MimeTypeUtils;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(JUnit4.class)
public class CarRegistryServiceControllerTest {

    private MockMvc mockMvc;

    private CarRegistryService carRegistryService =  Mockito.mock(CarRegistryService.class);;

    private CarRegistryController carRegistryController;

    ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUp() {

    }

    @Test
    void entryFrancoisDupondAddRequestShouldReturnOk() throws Exception {
        carRegistryController = new CarRegistryController(carRegistryService);

        //Given Audi TT car  registration for Francois dupond
        CarRequest carRegisteringRequest = CarRequest.builder()
                .driverName("Francois Dupond")
                .brand("Telsa")
                .color("red")
                .build();
        String carRequestString = objectMapper.writeValueAsString(carRegisteringRequest);

        //when registration is posted
        mockMvc = MockMvcBuilders
                .standaloneSetup(carRegistryController)
                .build();
      ResultActions resultAction = mockMvc.perform(MockMvcRequestBuilders.post("/api/car")
                .contentType(MediaType.APPLICATION_JSON)
                .content(carRequestString));

        //Entry with Francois dupond name should be in database
        resultAction.andExpect(status().isCreated());
    }

    @Test
    void shouldReturnAllCars() throws Exception {
        carRegistryController = new CarRegistryController(carRegistryService);

        //Given one entry with name = francois in database
        CarResponse response = CarResponse.builder()
                .driverName("Francois").build();
        List<CarResponse> responseList = Arrays.asList(response);
        doReturn(responseList)
                .when(carRegistryService).getAllCar();

        //when request made for all cars executed
        mockMvc = MockMvcBuilders
                .standaloneSetup(carRegistryController)
                .build();
        ResultActions resultAction = mockMvc.perform(MockMvcRequestBuilders.get("/api/cars")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE));


        //Return status should be Ok
        resultAction.andExpect(status().isOk());
        resultAction.andExpect(jsonPath("$.length()").value(1));
        resultAction.andExpect(jsonPath("$[?(@.driverName === 'Francois')]").exists());
    }

    @Test
    void shouldReturnSpecifiedEntry() throws Exception {
        carRegistryController = new CarRegistryController(carRegistryService);

        //Given one entry with name = francois in database
        CarResponse response = CarResponse.builder()
                .imatriculation("TOTOX12").build();
        doReturn(response)
                .when(carRegistryService).getCar("TOTOX12");

        //when request made for all cars executed
        mockMvc = MockMvcBuilders
                .standaloneSetup(carRegistryController)
                .build();
        ResultActions resultAction = mockMvc.perform(MockMvcRequestBuilders.get("/api/car/TOTOX12")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE));


        //Return status should be Ok
        resultAction.andExpect(status().isAccepted());
        resultAction.andExpect(jsonPath("$.imatriculation", is("TOTOX12")));
    }

    @Test
    void shouldUpdateSpecifiedEntryWithValues() throws Exception {
        carRegistryController = new CarRegistryController(carRegistryService);

        //Given Audi TT car  registration for Francois dupond
        CarRequest carRegisteringRequest = CarRequest.builder()
                .driverName("Francois Dupond")
                .brand("Telsa")
                .color("violet")
                .imatriculation("TOTOX12")
                .build();
        String carRequestString = objectMapper.writeValueAsString(carRegisteringRequest);

        //when registration is posted
        mockMvc = MockMvcBuilders
                .standaloneSetup(carRegistryController)
                .build();
        ResultActions resultAction = mockMvc.perform(MockMvcRequestBuilders.post("/api/carModify")
                .contentType(MediaType.APPLICATION_JSON)
                .content(carRequestString));

        //Return status should be ACCEPTED
        resultAction.andExpect(status().isAccepted());
    }

    @Test
    void shouldDeleteSpecifiedEntryWithName() throws Exception {
        carRegistryController = new CarRegistryController(carRegistryService);

        //when request made for all cars executed
        mockMvc = MockMvcBuilders
                .standaloneSetup(carRegistryController)
                .build();
        ResultActions resultAction = mockMvc.perform(MockMvcRequestBuilders.post("/api/deleteCar/Francois")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE));

        //Return status should be Ok
        resultAction.andExpect(status().isOk());
    }


}
