package com.EGEvaluation.carRegistryservice;

import com.EGEvaluation.carRegistryservice.dto.CarRequest;
import com.EGEvaluation.carRegistryservice.repository.CarRepository;
import com.EGEvaluation.carRegistryservice.service.CarRegistryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.spock.Testcontainers;

import java.util.List;

@SpringBootTest
@Testcontainers
public class CarRegistryServiceServiceTest {
    @Autowired
    private CarRepository carRepository;

    private static final MySQLContainer mySQLContainer;

    static {
        mySQLContainer = (MySQLContainer)(new MySQLContainer("mysql:8.0")
                .withUsername("testcontainers")
                .withPassword("Testcontain3rs!")
                .withReuse(true));
        mySQLContainer.start();
    }

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mySQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", mySQLContainer::getPassword);
        registry.add("spring.datasource.username", mySQLContainer::getUsername);
    }

    @Test
    void entryFrancoisDupondAddRequestShouldReturnOk() throws Exception {
        CarRegistryService service = new CarRegistryService(carRepository);
        carRepository.deleteAll();
        //Given Audi TT car  registration for Francois dupond
        CarRequest carRegisteringRequest = CarRequest.builder()
            .driverName("Francois Dupond")
            .brand("Telsa")
            .color("red")
                .year(1999L)
            .build();

        //when service treating add of car
        service.createCarEntry(carRegisteringRequest);

        //car should be added in database
        Assertions.assertEquals(1, carRepository.findAll().size());
        Assertions.assertEquals("Francois Dupond", carRepository.findAll().get(0).getDriverName());

     }

    @Test
    void shouldReceivedAllEntryFromDataBase() {
        carRepository.deleteAll();
        //Given Audi TT car  registration for Francois dupond in dataBAse
        CarRequest carRegisteringRequest = CarRequest.builder()
                .driverName("Francois Dupond")
                .brand("Telsa")
                .color("red")
                .year(1999L)
                .build();
        CarRegistryService service = new CarRegistryService(carRepository);
        service.createCarEntry(carRegisteringRequest);

        //car should be retrieved from database
        Assertions.assertEquals(1, carRepository.findAll().size());
        Assertions.assertEquals("Francois Dupond", carRepository.findAll().get(0).getDriverName());
    }

    @Test
    void shouldModifyEntryInDataBaseWithLicenseNumber() {
        carRepository.deleteAll();
        CarRegistryService service = new CarRegistryService(carRepository);

        //Given Audi TT car  registration for Francois dupond in dataBAse
        CarRequest carRegisteringRequest = CarRequest.builder()
                .driverName("Francois Dupond")
                .brand("Telsa")
                .color("red")
                .year(1999L)
                .build();
        service.createCarEntry(carRegisteringRequest);
        String imatriculation = carRepository.findAll().get(0).getImatriculation();

        //when modification request for color from red to violet
        CarRequest carRegisteringRequest2 = CarRequest.builder()
                .driverName("Francois Dupond")
                .brand("Telsa")
                .color("violet")
                .imatriculation(imatriculation)
                .build();
        service.modifyCar(carRegisteringRequest2);

        //car should be updated in database
        Assertions.assertEquals(1, carRepository.findAll().size());
        Assertions.assertEquals("violet", carRepository.findAll().get(0).getColor());
    }


    @Test
    void entryFrancoisDupondShouldDeletedFromDataBaseAfterAdd() {
        carRepository.deleteAll();
        CarRegistryService service = new CarRegistryService(carRepository);

        //Given Audi TT car  registration for Francois dupond in dataBAse
        CarRequest carRegisteringRequest = CarRequest.builder()
                .driverName("Francois Dupond")
                .brand("Telsa")
                .color("red")
                .year(1999L)
                .build();
        service.createCarEntry(carRegisteringRequest);
        String imatriculation = carRepository.findAll().get(0).getImatriculation();


        //when suppression request for car entry
        service.deleteCarEntry(imatriculation);

        //car should be removed from database
        Assertions.assertEquals(0, carRepository.findAll().size());
    }

}
