package com.EGEvaluation.carRegistryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarRegistryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarRegistryServiceApplication.class, args);
	}

}
