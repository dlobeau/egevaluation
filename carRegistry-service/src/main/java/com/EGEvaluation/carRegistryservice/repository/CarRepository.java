package com.EGEvaluation.carRegistryservice.repository;

import com.EGEvaluation.carRegistryservice.model.Car;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public interface CarRepository extends JpaRepository<Car, Long> {
    List<Car> findByImatriculation(String imatriculation);

    @Transactional
    @Modifying
    @Query("delete from Car c where c.id = ?1")
    void deleteById(Long id);
}
