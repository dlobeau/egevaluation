package com.EGEvaluation.carRegistryservice.controller;

import com.EGEvaluation.carRegistryservice.dto.CarRequest;
import com.EGEvaluation.carRegistryservice.dto.CarResponse;
import com.EGEvaluation.carRegistryservice.service.CarRegistryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CarRegistryController {

    private final CarRegistryService carRegistryService;

    @RequestMapping(value = "/car", method = POST)
    @ResponseStatus(HttpStatus.CREATED)
    public String createCarEntry(@RequestBody CarRequest carRequest) {
        carRegistryService.createCarEntry(carRequest);
        return "car entry Successfully added";
    }

    @RequestMapping(value = "/cars", method = GET)
    @ResponseStatus(HttpStatus.OK)
    public List<CarResponse> getListCarEntry() {
        return carRegistryService.getAllCar();
    }

    @RequestMapping(value = "/car/{name}", method = GET)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public CarResponse getCarEntry(@PathVariable("name") String name) {
      return carRegistryService.getCar(name);
    }

    @RequestMapping(value = "/carModify", method = POST)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public String modifyCarEntry(@RequestBody CarRequest carRequest) {
         carRegistryService.modifyCar(carRequest);
        return "car entry Successfully modified";
    }

    @RequestMapping(value = "/deleteCar/{name}", method = POST)
    @ResponseStatus(HttpStatus.OK)
    public String deleteCarEntry(@PathVariable("name") String name) {
        carRegistryService.deleteCarEntry(name);

        return "car entry Successfully deleted";
    }
}
