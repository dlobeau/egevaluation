package com.EGEvaluation.carRegistryservice.model;

import org.hibernate.Session;
import org.hibernate.tuple.ValueGenerator;

public class ImatriculationCodeGenerator implements ValueGenerator<String> {

        @Override
        public String generateValue(Session session, Object owner) {
            Car car = (Car) owner;
            String code = car.getYear().toString()+"_"
                    +car.getDriverName().replace(" ", "");
            return code;
        }


}
