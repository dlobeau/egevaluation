package com.EGEvaluation.carRegistryservice.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;

@Entity
@Table(name = "t_cars")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String brand;
    String color;
    Long year;
    String driverName;
    String driverAddress;

    @GeneratorType(type = ImatriculationCodeGenerator.class,
            when = GenerationTime.INSERT)
     String imatriculation;
}
