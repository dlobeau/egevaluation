package com.EGEvaluation.carRegistryservice.service;

import com.EGEvaluation.carRegistryservice.dto.CarRequest;
import com.EGEvaluation.carRegistryservice.dto.CarResponse;
import com.EGEvaluation.carRegistryservice.model.Car;
import com.EGEvaluation.carRegistryservice.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional

public class CarRegistryService {
    private final CarRepository carRepository;

    public void createCarEntry(CarRequest carRequest) {
        Car newCar = mapCar(carRequest);
        carRepository.save(newCar);
    }

    private Car mapCar(CarRequest carRequest) {
        Car newCar = new Car();
        newCar.setColor(carRequest.getColor());
        newCar.setBrand(carRequest.getBrand());
        newCar.setDriverName(carRequest.getDriverName());
        newCar.setYear(carRequest.getYear());
        newCar.setDriverAddress(carRequest.getDriverAddress());
        return newCar;
    }

    public List<CarResponse> getAllCar() {
        List<Car> cars = carRepository.findAll();
        return cars.stream().map(this::mapToProductResponse).toList();
    }

    public CarResponse getCar(String imatriculation) {
        List<Car> cars = carRepository.findByImatriculation(imatriculation);
        if( !cars.isEmpty()) {
            return mapToProductResponse(cars.get(0));
        }
        return null;
    }

    private CarResponse mapToProductResponse(Car car) {
        return CarResponse.builder()
                .id(car.getId())
                .brand(car.getBrand())
                .color(car.getColor())
                .driverName(car.getDriverName())
                .year(car.getYear())
                .imatriculation(car.getImatriculation())
                .driverAddress(car.getDriverAddress())
                .build();
    }

    public void modifyCar(CarRequest carRequest) {
        List<Car> cars = carRepository.findByImatriculation(carRequest.getImatriculation());
        if(Objects.nonNull(cars) && !cars.isEmpty()) {
            Car car = cars.get(0);
            car.setYear(carRequest.getYear());
            car.setDriverName(carRequest.getDriverName());
            car.setBrand(carRequest.getBrand());
            car.setColor(carRequest.getColor());
            car.setDriverAddress(carRequest.getDriverAddress());
            car.setImatriculation(car.getImatriculation());
            carRepository.save(car);
        }
    }

    public void deleteCarEntry(String imatriculation) {
        List<Car> cars = carRepository.findByImatriculation(imatriculation);
        if(Objects.nonNull(cars) && !cars.isEmpty()) {
            Car car = cars.get(0);
            carRepository.deleteById(car.getId());
            carRepository.flush();
        }
    }
}
