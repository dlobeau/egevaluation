package com.EGEvaluation.carRegistryservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CarResponse {
    Long id;
    String brand;
    String color;
    Long year;
    String driverName;
    String driverAddress;
    String imatriculation;
}
