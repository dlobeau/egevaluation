package com.EGEvaluation.fineservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebConfigurationConfig {

    @Bean
    public WebClient webClient() {
        return WebClient.builder().build();
    }

}
