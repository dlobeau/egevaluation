package com.EGEvaluation.fineservice.service;

import com.EGEvaluation.fineservice.dto.CarResponse;
import com.EGEvaluation.fineservice.dto.FineRequest;
import com.EGEvaluation.fineservice.dto.FineResponse;
import com.EGEvaluation.fineservice.model.Fine;
import com.EGEvaluation.fineservice.repository.FineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Transactional
public class FineServiceService {

    private final FineRepository fineRepository;
    private final WebClient webClient;

    public void createFineEntry(FineRequest fineRequest) {
        Fine newFine = mapFine(fineRequest);

        CarResponse carResponse = getCarAdditionalInformation(newFine.getImatriculation());

        if(Objects.nonNull(carResponse)) {
            newFine.setDriverName(carResponse.getDriverName());
            newFine.setDriverAddress(carResponse.getDriverAddress());
        }

        fineRepository.save(newFine);
    }

    public CarResponse getCarAdditionalInformation(String imatriculation) {
        //get driverName and driverAddress from carRegistry service
        CarResponse carResponse = webClient.get().uri("http://localhost:8081/api/car/"+ imatriculation)
                .retrieve()
                .bodyToMono(CarResponse.class)
                .block();
        return carResponse;
    }

    private Fine mapFine(FineRequest fineRequest) {
        Fine newFine = new Fine();
        newFine.setAmount(fineRequest.getAmount());
        newFine.setDate(fineRequest.getDate());
        newFine.setDriverName(fineRequest.getDriverName());
        newFine.setImatriculation(fineRequest.getImmatriculation());
        newFine.setLocation(fineRequest.getLocation());
        newFine.setDriverAddress(fineRequest.getDriverAdress());
        return newFine;
    }

    public List<FineResponse> getFineEntries() {
        List<Fine> fines = fineRepository.findAll();
        return fines.stream().map(this::mapToFineResponse).toList();
    }

    private FineResponse mapToFineResponse(Fine fine) {
        return FineResponse.builder()
                .immatriculation(fine.getImatriculation())
                .driverName(fine.getDriverName())
                .date(fine.getDate())
                .amount(fine.getAmount())
                .driverAdress(fine.getDriverAddress())
                .location(fine.getLocation())
                .build();
    }
}
