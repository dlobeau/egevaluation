package com.EGEvaluation.fineservice.controller;

import com.EGEvaluation.fineservice.dto.FineRequest;
import com.EGEvaluation.fineservice.dto.FineResponse;
import com.EGEvaluation.fineservice.service.FineServiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class FineServiceController {

     final private FineServiceService fineServiceService;

     @RequestMapping(value = "/fine", method = POST)
     @ResponseStatus(HttpStatus.CREATED)
     public String createFineEntry(@RequestBody FineRequest fineRequest) {


          fineServiceService.createFineEntry(fineRequest);
          return "Fine entry Successfully added";
     }

     @RequestMapping(value = "/fines", method = GET)
     @ResponseStatus(HttpStatus.OK)
     public List<FineResponse> getFineEntries() {

          return fineServiceService.getFineEntries();
     }

}
