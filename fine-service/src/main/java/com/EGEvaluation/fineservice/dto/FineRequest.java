package com.EGEvaluation.fineservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FineRequest {
    String date;
    Long amount;
    String location;
    String immatriculation;
    String driverAdress;
    String driverName;
}
