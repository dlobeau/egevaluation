package com.EGEvaluation.fineservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FineResponse {
    String date;
    Long amount;
    String location;
    String immatriculation;
    String driverAdress;
    String driverName;
}
