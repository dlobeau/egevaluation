package com.EGEvaluation.fineservice.repository;

import com.EGEvaluation.fineservice.model.Fine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FineRepository extends JpaRepository<Fine, Long> {
}
