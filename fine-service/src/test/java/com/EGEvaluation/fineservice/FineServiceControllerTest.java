package com.EGEvaluation.fineservice;

import com.EGEvaluation.fineservice.controller.FineServiceController;
import com.EGEvaluation.fineservice.dto.FineRequest;
import com.EGEvaluation.fineservice.dto.FineResponse;
import com.EGEvaluation.fineservice.service.FineServiceService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.MimeTypeUtils;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class FineServiceControllerTest {

    private FineServiceController fineServiceController;

    private FineServiceService fineServiceService =  Mockito.mock(FineServiceService.class);;

    private ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;


    @Test
    void entryFrancoisDupondAddRequestShouldReturnOk() throws Exception {
        fineServiceController = new FineServiceController(fineServiceService);

        //Given driver with immatriculation 95ZRTTT
        FineRequest fineRegisteringRequest = FineRequest.builder()
                .immatriculation("95ZRTTT")
                .driverAdress("224 calle urgel")
                .amount(125L)
                .location("Aragon y Aribau Barcelona")
                .date("2020-11-25T10:00")
                .build();
        String carRequestString = objectMapper.writeValueAsString(fineRegisteringRequest);

        //when registration is posted
        mockMvc = MockMvcBuilders
                .standaloneSetup(fineServiceController)
                .build();
        ResultActions resultAction = mockMvc.perform(MockMvcRequestBuilders.post("/api/fine")
                .contentType(MediaType.APPLICATION_JSON)
                .content(carRequestString));

        //Entry should be added in database
        resultAction.andExpect(status().isCreated());
    }

    @Test
    void shouldReturnAllFines() throws Exception {
        fineServiceController = new FineServiceController(fineServiceService);

        //Given one entry with name = francois in database
        FineResponse response = FineResponse.builder()
                .driverName("Francois")
                .immatriculation("12345").build();
        List<FineResponse> responseList = Arrays.asList(response);
        doReturn(responseList)
                .when(fineServiceService).getFineEntries();

        //when request made for all cars executed
        mockMvc = MockMvcBuilders
                .standaloneSetup(fineServiceController)
                .build();
        ResultActions resultAction = mockMvc.perform(MockMvcRequestBuilders.get("/api/fines")
                .accept(MimeTypeUtils.APPLICATION_JSON_VALUE));


        //Return status should be Ok
        resultAction.andExpect(status().isOk());
        resultAction.andExpect(jsonPath("$.length()").value(1));
        resultAction.andExpect(jsonPath("$[?(@.driverName === 'Francois')]").exists());
    }
}
