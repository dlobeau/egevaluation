package com.EGEvaluation.fineservice;

import com.EGEvaluation.fineservice.dto.FineRequest;
import com.EGEvaluation.fineservice.repository.FineRepository;
import com.EGEvaluation.fineservice.service.FineServiceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.web.reactive.function.client.WebClient;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest
@Testcontainers
public class FineServiceServiceTest {
    @Autowired
    private FineRepository fineRepository;

    @Autowired
    private WebClient webClient;

    private static final MySQLContainer mySQLContainer;

    static {
        mySQLContainer = (MySQLContainer)(new MySQLContainer("mysql:8.0")
                .withUsername("testcontainers")
                .withPassword("Testcontain3rs!")
                .withReuse(true));
        mySQLContainer.start();
    }

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", mySQLContainer::getJdbcUrl);
        registry.add("spring.datasource.password", mySQLContainer::getPassword);
        registry.add("spring.datasource.username", mySQLContainer::getUsername);
    }

    @Test
    void newFineEntryAddRequestShouldReturnOk() throws Exception {
        FineServiceService  service = new FineServiceService(fineRepository,webClient);


        //Given Audi TT car fine registration for Francois dupond
        FineRequest fineRegisteringRequest = FineRequest.builder()
                .immatriculation("95ZRTTT")
                .driverAdress("224 calle urgel")
                .amount(125L)
                .location("Aragon y Aribau Barcelona")
                .date("2020-11-25T10:00")
                .driverName("toto")
                .build();

        //when service treating add of fine
        service.createFineEntry(fineRegisteringRequest);

        //car should be added in database
        Assertions.assertEquals(1, fineRepository.findAll().size());
        Assertions.assertEquals("95ZRTTT", fineRepository.findAll().get(0).getImatriculation());

    }
}
